import React from 'react'
import '../styles/Car.css'

import StarRatingComponent from 'react-star-rating-component'
import ReactMapGL, {Marker} from 'react-map-gl';

export default function() {

  return(
    <div>

      <div className="panel panel-default">
        <div className="panel-footer">{this.state.car.name}</div>
        <div className="panel-body">
          <div>
            <StarRatingComponent
              name="rate"
              starCount={5}
              value={parseInt(this.state.car.rate_average)}
              onStarClick={(nextValue) => this.onStarClick(this.state.car.id, nextValue)}
            />
            <p>{"Weight: " + this.state.car.weight}</p>
            <p>{"Color: " + this.state.car.color}</p>
          </div>
          <img
            className="car-image"
            src={"http://localhost:3000" + this.state.car.image}
          />
          <br />
          <br />
          <ReactMapGL
            {...this.state.viewport}
            onViewportChange={(viewport) => this.setState({viewport})}
            mapboxApiAccessToken="pk.eyJ1IjoiYXR0aWxhc3playIsImEiOiJjampxdDRlOTY4YzNuM3BwMWQzYXNwem80In0.J3mwOlnRZWN4EC9W658cbg"
          >
            <Marker 
              key={this.state.car.id}
              latitude={this.state.car.latitude}
              longitude={this.state.car.longitude}
            >
              <span className="glyphicon glyphicon-flag" style={{color: this.state.car.color}}></span>

            </Marker>
          </ReactMapGL>
        </div>
      </div>

    </div>
  )
}