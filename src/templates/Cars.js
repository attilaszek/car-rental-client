import React from 'react'
import '../styles/Cars.css'

import { Link } from 'react-router-dom'
import StarRatingComponent from 'react-star-rating-component'
import ReactMapGL, {Marker} from 'react-map-gl';

export default function() {
  return(
    <div>
      <ReactMapGL
        {...this.state.viewport}
        onViewportChange={(viewport) => this.setState({viewport})}
        mapboxApiAccessToken="pk.eyJ1IjoiYXR0aWxhc3playIsImEiOiJjampxdDRlOTY4YzNuM3BwMWQzYXNwem80In0.J3mwOlnRZWN4EC9W658cbg"
      >
        {this.props.cars.map((car, index) =>
          (
            <Marker 
              key={car.id}
              latitude={car.latitude}
              longitude={car.longitude}
            >
              <span className="glyphicon glyphicon-flag" style={{color: car.color}}></span>

            </Marker>
          )
        )}
      </ReactMapGL>


      <div id='searchDiv'>
        <h3>Filter cars by:</h3>
        <br />

        <form>

            <div className={"form-group row " + (this.state.start_date_error && "has-error")}>
              <label htmlFor="start_date" className="col-sm-4 col-form-label">Start date</label>
              <div className="col-sm-8">
                <input 
                  type="date"
                  className="form-control"
                  name="start_date"
                  placeholder="" 
                  value={this.state.start_date}
                  onChange={this.handleInputChange}
                />
              </div>
              {this.state.start_date_error && <p className="error-label">{this.state.start_date_error[0]}</p>}

            </div>

            <div className={"form-group row " + (this.state.end_date_error && "has-error")}>
              <label htmlFor="end_date" className="col-sm-4 col-form-label">End date</label>
              <div className="col-sm-8">
                <input 
                  type="date"
                  className="form-control"
                  name="end_date"
                  placeholder=""
                  value={this.state.end_date}
                  onChange={this.handleInputChange}
                />
              </div>
              {this.state.end_date_error && <p className="error-label">{this.state.end_date_error[0]}</p>}
            </div>

            <div className="form-group row">
              <label htmlFor="name" className="col-sm-4 col-form-label">Name</label>
              <div className="col-sm-8">
              <input 
                type="text"
                className="form-control"
                name="name"
                placeholder=""
                value={this.state.name}
                onChange={this.handleInputChange}
                />
              </div>
            </div>

            <div className="form-group row">
              <label htmlFor="colorSelect" className="col-sm-4 col-form-label">Color</label>
              <div className="col-sm-8">
                <select 
                  className="form-control"
                  name="color"
                  value={this.state.color}
                  onChange={this.handleInputChange}
                >
                  <option value={null}></option>
                  {this.state.colors.map(color =>
                    <option key={color} value={color}>{color}</option>
                  )}
                </select>
              </div>
            </div>

            <div className="form-group row">
              <label htmlFor="weight_min" className="col-sm-4 col-form-label">Min weight</label>
              <div className="col-sm-8">
                <input
                  type="number"
                  className="form-control"
                  name="weight_min"
                  placeholder=""
                  value={this.state.weight_min}
                  onChange={this.handleInputChange}
                />
              </div>
            </div>

            <div className="form-group row">
              <label htmlFor="weight_max" className="col-sm-4 col-form-label">Max weight</label>
              <div className="col-sm-8">
                <input
                  type="number"
                  className="form-control"
                  name="weight_max"
                  placeholder=""
                  value={this.state.weight_max}
                  onChange={this.handleInputChange}
                />
              </div>
            </div>

        </form>
      </div>


      <div id="contentDiv"> 
        <div id="titleDiv"><h3>List of available cars</h3></div>
        <div id="addBtnDiv">
          {this.props.user.admin && <Link className="btn btn-default" to={'/cars/new'}>Add new car</Link>}
        </div>

        <table className="table table-hover">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Name</th>
              <th scope="col">Color</th>
              <th scope="col">Weight</th>
              <th scope="col">Rate avg</th>
              {this.props.user.admin && <th scope="col">Delete</th>}
              {this.props.user.admin && <th scope="col">Edit</th>}
              <th scope="col">Reserve</th>
            </tr>
          </thead>
          <tbody>
            {this.props.cars.map((car, index) =>
              <tr key={car.id}>
                <th scope="row">{index + 1}</th>
                <td><Link to={'/cars/' + car.id}>{car.name}</Link>  </td>
                <td>{car.color}</td>
                <td>{car.weight}</td>
                <td>
                  <StarRatingComponent
                    name={"rate"+car.id}
                    starCount={5}
                    value={parseInt(car.rate_average)}
                    onStarClick={(nextValue) => this.onStarClick(car.id, nextValue)}
                  />
                </td>
                {this.props.user.admin &&
                  <td><a onClick={() => {this.deleteCar(car.id)}}>Delete</a></td>
                }
                {this.props.user.admin &&
                  <td><Link to={'/cars/edit/' + car.id}>Edit</Link></td>
                }
                <td><a onClick={() => {this.reserveCar(car.id)}}>Reserve</a></td>
              </tr>
            )}
          </tbody>
        </table>
      </div>
    </div>
  )
}