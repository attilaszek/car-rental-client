import React from 'react'
import '../styles/Home.css'
import image from '../images/rent-a-car.jpg'

export default function() {
  return(
    <div className="centeredImg" alt="">
      <img src={image} />
    </div>
  )
}