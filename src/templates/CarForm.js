import React from 'react'
import '../styles/NewCar.css'

export default function() {

  return(
    <div>
      <div className={"form-group " + (this.state.errors.name && "has-error")}>
        <label htmlFor="name" className="col-sm-4 control-label">Name</label>
        <div className="col-sm-8">
          <input
            type="text"
            className="form-control"
            name="name"
            placeholder="Name" 
            value={this.state.car.name}
            onChange={this.handleInputChange}
            />
        </div>
        <p className="errorLabel col-sm-offset-4">{this.state.errors.name && this.state.errors.name[0]}</p>
      </div>

      <div className={"form-group " + (this.state.errors.color && "has-error")}>
        <label htmlFor="colorSelect" className="col-sm-4 control-label">Color</label>
        <div className="col-sm-8">
          <select 
            className="form-control"
            name="color"
            value={this.state.car.color}
            onChange={this.handleInputChange}
          >
            <option value={null}></option>
            {this.state.colors.map(color =>
              <option key={color} value={color}>{color}</option>
            )}
          </select>
        </div>
        <p className="errorLabel col-sm-offset-4">{this.state.errors.color && this.state.errors.color[0]}</p>
      </div>

      <div className={"form-group " + (this.state.errors.weight && "has-error")}>
        <label htmlFor="weight" className="col-sm-4 control-label">Weight</label>
        <div className="col-sm-8">
          <input
            type="number"
            className="form-control"
            name="weight"
            placeholder="Weight" 
            value={this.state.car.weight}
            onChange={this.handleInputChange}
            />
        </div>
        <p className="errorLabel col-sm-offset-4">{this.state.errors.weight && this.state.errors.weight[0]}</p>
      </div>

      <div className={"form-group " + (this.state.errors.latitude && "has-error")}>
        <label htmlFor="latitude" className="col-sm-4 control-label">Latitude</label>
        <div className="col-sm-8">
          <input
            type="number"
            className="form-control"
            name="latitude"
            placeholder="Latitude" 
            value={this.state.car.latitude}
            onChange={this.handleInputChange}
            />
        </div>
        <p className="errorLabel col-sm-offset-4">{this.state.errors.latitude && this.state.errors.latitude[0]}</p>
      </div>

      <div className={"form-group " + (this.state.errors.longitude && "has-error")}>
        <label htmlFor="longitude" className="col-sm-4 control-label">Longitude</label>
        <div className="col-sm-8">
          <input
            type="number"
            className="form-control"
            name="longitude"
            placeholder="Longitude" 
            value={this.state.car.longitude}
            onChange={this.handleInputChange}
            />
        </div>
        <p className="errorLabel col-sm-offset-4">{this.state.errors.longitude && this.state.errors.longitude[0]}</p>
      </div>

      <div className="form-group">
        <div className="col-sm-offset-3 col-sm-11">
          <label className="btn btn-default col-sm-6">
            <div>Upload image</div>
            <input type="file" style={{display: "none"}} name="fileName" onChange={this.handleFileChange}></input>
          </label>
        </div>
        <center>{this.state.image && this.state.image.name}</center>
      </div>
    </div>
  )
}