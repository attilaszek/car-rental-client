import React from 'react'
import '../styles/NewCar.css'

import { Redirect } from 'react-router-dom'
import carFormTemplate from './CarForm.js'

export default function() {

  return(
    <div id="formPanel" className="panel panel-default">
      <div className="panel-heading">
        <h3 className="panel-title">Add new car</h3>
      </div>
      <div className="panel-body">
        <form className="form-horizontal">

          {carFormTemplate.call(this)}

          <div className="form-group">
            <div className="col-sm-offset-4 col-sm-10">
              <div
                className="btn btn-default col-sm-4"
                onClick={this.handleSubmit}
              >
                Create car
              </div>
            </div>
          </div>

        </form>
      </div>

      {this.state.fireRedirect && (
        <Redirect to={'/cars'}/>
      )}

    </div>
  )
}