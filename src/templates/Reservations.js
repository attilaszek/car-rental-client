import React from 'react'
import '../styles/Reservations.css'

import { Link } from 'react-router-dom'

export default function() {
  return(
    <div>
      <h3>Reservations</h3>

      <table className="table table-hover">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Car</th>
            <th scope="col">Start date</th>
            <th scope="col">End date</th>
            <th scope="col">Cancel reservation</th>
          </tr>
        </thead>
        <tbody>
          {this.state.reservations.map((reservation, index) =>
            <tr key={reservation.id}>
              <th scope="row">{index + 1}</th>
              <td><Link to={'/cars/' + reservation.car_id}>{reservation.car_name}</Link>  </td>
              <td>{reservation.start_date}</td>
              <td>{reservation.end_date}</td>
              <td><a onClick={() => {this.cancelReservation(reservation.id)}}>Cancel</a></td>
            </tr>
          )}
        </tbody>
      </table>

    </div>
  )
}