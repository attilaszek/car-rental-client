import React from 'react'
//import '../styles/Reservations.css'

import { Link } from 'react-router-dom'

export default function() {
  return(
    <div>
      <table className="table table-hover">
        <tbody>
          {this.state.reservations.map((reservation, index) =>
            <tr key={reservation.id}>
              <td>{reservation.start_date}</td>
              <td>-></td>
              <td>{reservation.end_date}</td>
            </tr>
          )}
        </tbody>
      </table>

    </div>
  )
}