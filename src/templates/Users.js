import React from 'react'
import '../styles/Users.css'

import { Link } from 'react-router-dom'

export default function() {
  return(
    <div  id="contentDiv">
      <div id="titleDiv"><h3>Users list</h3></div>
      <div id="addBtnDiv">
        <Link className="btn btn-default" to={'/users/new'}>Add new user</Link>
      </div>
      <br />

      <table className="table table-hover">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">First name</th>
            <th scope="col">Last name</th>
            <th scope="col">Email</th>
            <th scope="col">Admin</th>
            <th scope="col">Delete</th>
            <th scope="col">Edit</th>
          </tr>
        </thead>
        <tbody>
          {this.state.users.map((user, index) =>
            <tr key={user.id}>
              <th scope="row">{index + 1}</th>
              <td>{user.first_name}</td>
              <td>{user.last_name}</td>
              <td>{user.email}</td>
              <td>
                <input
                  type="checkbox"
                  checked={user.admin}
                  onChange={(event) => {this.handleChange(event, user.id)}}
                />
              </td>
              <td><a onClick={() => {this.deleteUser(user.id)}}>Delete</a></td>
              <td><Link to={"users/edit/" + user.id}>Edit</Link></td>
            </tr>
          )}
        </tbody>
      </table>
    </div>
  )
}