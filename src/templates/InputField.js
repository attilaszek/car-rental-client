import React from 'react'

export default function(type, model, value, error_msg) {
  //alert("1: " + type + "\n2: " + model + "\n3: " + value + "\n4: " + error_msg)
  return(
    <div className={"form-group " + (error_msg && error_msg.length > 0 && "has-error")} key={model}>
      <input 
        type={type}
        className="form-control"
        name={model}
        placeholder={model.replace(/_/g, ' ')}
        value={value}
        onChange={this.handleInputChange}
      />
      <p className="errorLabel">{error_msg && error_msg.map(msg => msg + "; ")}</p>
    </div>
  )
}