import React from 'react'
import '../styles/CarsQuery.css'
import ReservationsSub from '../components/ReservationsSub.js'

import StarRatingComponent from 'react-star-rating-component'

export default function() {
  return(
    <div id="car-container">
      <h3>Reserved cars</h3>
      <h5>Showing a list of cars, reserved by the selected user</h5>
      <br />

      <div className="form-group  col-sm-6">
        <label htmlFor="userSelect">Select user:</label>
        <select
          className="form-control"
          name="user_id"
          value={this.state.user_id}
          onChange={this.handleInputChange}
        >
          <option value={null}></option>
          {this.state.users.map(user =>
            <option key={user.id} value={user.id}>{user.first_name + " " + user.last_name}</option>
          )}
        </select>
      </div>

      {this.state.user_id && this.state.cars.length == 0 &&
        <div className="col-sm-12">
          <h5>No reserved cars by this user</h5>
        </div>
      }


      {this.state.user_id &&this.state.cars.length > 0 && <table className="table table-hover">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Weight</th>
            <th scope="col">Rate average</th>
            <th scope="col">Intervals</th>
          </tr>
        </thead>
        <tbody>
          {this.state.cars.map((car, index) =>
            <tr key={car.id}>
              <th scope="row">{index + 1}</th>
              <td>{car.name}</td>
              <td>{car.weight}</td>
              <td>
                <StarRatingComponent
                  name="rate"
                  starCount={5}
                  value={parseInt(car.rate_average)}
                />
              </td>
              <td><ReservationsSub car_id={car.id} /></td>
            </tr>

          )}
        </tbody>
      </table>
    }
    </div>
  )
}
