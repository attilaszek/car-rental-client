import React from 'react'
// import '../styles/UsersQuery.css'
import ReservationsSub from '../components/ReservationsSub.js'

export default function() {
  return(
    <div>
      <h3>Users with reservations</h3>
      <br />

      <table className="table table-hover">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">First name</th>
            <th scope="col">Last name</th>
            <th scope="col">Email</th>
            <th scope="col">Intervals</th>
          </tr>
        </thead>
        <tbody>
          {this.state.users.map((user, index) =>
            <tr key={user.id}>
              <th scope="row">{index + 1}</th>
              <td>{user.first_name}</td>
              <td>{user.last_name}</td>
              <td>{user.email}</td>
              <td><ReservationsSub user_id={user.id} /></td>
            </tr>

          )}
        </tbody>
      </table>
    </div>
  )
}
