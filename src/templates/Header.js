import React, { Component } from 'react';
import '../styles/Header.css';

import { Link } from 'react-router-dom';

export default function(userSection) {
  return(
    <div>
      <nav className="navbar navbar-fixed-top navbar-default justify-content-between">
        <div className="container">
          <div className="navbar-header">
            <Link to='/' className="navbar-brand">Car Rental</Link>  
            <ul className="nav navbar-nav">
              {this.props.user && (
                <ul className="nav navbar-nav">
                  <li><Link to='/cars' className="navbar-item">Cars</Link></li>
                  <li><Link to='/reservations' className="navbar-item">Reservations</Link></li>
                </ul>
              )}
              {this.props.user && this.props.user.admin && (
                <ul className="nav navbar-nav">
                  <li><Link to='/users' className="navbar-item">Users</Link></li>
                  <li><Link to='/query/users' className="navbar-item">Users query</Link></li>
                  <li><Link to='/query/cars' className="navbar-item">Cars query</Link></li>
                </ul>
              )}
              {!this.props.user && (
                <li><Link to='/signup' className="nav-link">Signup</Link></li>
              )}
            </ul>
          </div>
                
          {userSection}
        </div>
      </nav>
    </div>
  )
}