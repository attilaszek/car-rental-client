import React from 'react'
import '../styles/Signup.css'

import inputFieldTemplate from './InputField.js'

export default function() {
  const errors = this.state.errors
  const user = this.state.user
  const header_text = this.state.new_user ? "Create new user" : "Update user"

  return(
    <div className="form panel panel-default" id="loginForm">
      <h3 className="centered">
        {this.props.user ? header_text : "Sign up"}
      </h3>
      <br />
      
      {inputFieldTemplate.call(this, "text", "first_name", user.first_name, errors.first_name)}
      {inputFieldTemplate.call(this, "text", "last_name", user.last_name, errors.last_name)}
      {inputFieldTemplate.call(this, "text", "email", user.email, errors.email)}
      {this.state.new_user && 
        inputFieldTemplate.call(this, "password", "password", user.password, errors.password)}
      {this.state.new_user &&
        inputFieldTemplate.call(this, "password", "password_confirmation", user.password_confirmation, errors.password_confirmation)}
      
      <div className="centered">
        <button className="btn btn-default" onClick={this.handleClick}>Submit</button>
      </div>
      {this.renderRedirect()}
    </div>
  )
}