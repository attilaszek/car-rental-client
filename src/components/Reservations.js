import React, { Component } from 'react'
import reservationsTemplate from '../templates/Reservations.js'

import myAxios from '../MyAxios.js'

import { connect } from 'react-redux';

class Reservations extends Component {

  state = {
    reservations: []
  }

  loadReservations = () => {
    const self = this
    myAxios.get('reservations/index.json')
    .then(function (response) {
      console.log(response)
      self.setState({
        reservations: response.data
      })
    })
  }

  cancelReservation = (id) => {
    const self = this
    myAxios.delete('reservations.json', {
      params: {
        id: id
      }
    }).then(function (response) {
      console.log(response)
      self.loadReservations()
    })
    .catch(function (error) {
      if (error.response) {
        var errors = error.response.data
        if (errors.error[0]) alert(errors.error[0].reservation)
      }
    })
  }

  componentWillMount() {
    this.loadReservations()
  }

  render() {
    return reservationsTemplate.call(this)
  }
}

const mapStateToProps = state => ({ 
  user: state.user
});

export default connect(mapStateToProps, {})(Reservations);