import React, { Component } from 'react';

import headerTemplate from '../templates/Header.js'
import Login from './Login.js';
import Logout from './Logout.js';

import { connect } from 'react-redux';

class Header extends Component {
  render() {
    const userSection = sessionStorage.getItem('jwtToken') && this.props.user ? (
      <Logout />
    ) : (
      <Login />
    );

    return headerTemplate.call(this, userSection)
  }
}

const mapStateToProps = state => {
  return { user: state.user };
};

export default connect(mapStateToProps)(Header);