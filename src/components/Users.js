import React, {Component} from 'react'
import usersTemplate from '../templates/Users.js'

import myAxios from '../MyAxios.js'

class Users extends Component {

  state = {
    users: []
  }

  getUsersList = () => {
    const self = this
    myAxios.get('users/index')
    .then(function (response) {
      console.log(response)
      self.setState({
        users: response.data
      })
    })
  }

  deleteUser = (id) => {
    const self = this
    myAxios.delete('users.json', {
      params: {
        id: id
      }
    }).then(function (response) {
      console.log(response)
      self.getUsersList()
    })
  }

  handleChange = (event, id) => {
    
    const command = event.target.checked ? 'add_admin_role' : 'remove_admin_role'

    const self = this
    myAxios.put('users/' + command +'.json', {
      id: id
    }).then(function (response) {
      console.log(response)
      self.getUsersList()
    })
    .catch(function (error) {
      if (error.response) {
        console.log(error.response)
        if (error.response.data.error[0]) alert(error.response.data.error[0].user)
      }
    })
  }

  componentWillMount() {
    this.getUsersList()
  }

  render() {
    return usersTemplate.call(this)
  }
}

export default Users