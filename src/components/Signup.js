import React, { Component } from 'react';
import myAxios from '../MyAxios.js'
import signupTemplate from '../templates/Signup.js'

import { setCurrentUser } from '../actions/index.js'
import { connect } from 'react-redux';
import update from 'react-addons-update'
import { Redirect } from 'react-router-dom'

class Signup extends Component {
  state = {
    user: { 
      first_name: "",
      last_name: "",
      email: "",
      password: "",
      password_confirmation: "",
    },
    errors: {
      first_name: [],
      last_name: [],
      email: [],
      password: [],
      password_confirmation: []
    },
    redirect: false,
    new_user: true,
  }

  handleInputChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      user: update(this.state.user, {
        [name]: {$set: value}
      })
    });
  }

  getCurrentUser = () => {
    const self = this

    myAxios.get('users/get_current_user.json')
    .then(response => {
      self.props.setCurrentUser(response.data)
    })
  }

  login = (auth_token) => {
    if (auth_token) {
      sessionStorage.setItem('jwtToken', auth_token);
      myAxios.defaults.headers.authorization = auth_token;
      this.getCurrentUser()
    }
  }

  renderRedirect = () => {
    if (this.state.redirect) {
      return <Redirect to='/users' />
    }
  }

  handleClick = () => {

    const self = this

    myAxios.post('users/signup.json', this.state.user)
    .then(function (response) {
      var auth_token = response.data.token
      if (self.props.user) {
        self.setState({
          redirect: true
        })
      }
      else {
        self.login(auth_token)
      }
      console.log(response)
    })
    .catch(function (error) {
      if (error.response) {
        var errors = error.response.data
        console.log(errors)
        self.setState({
          user: update(self.state.user, {
            password: {$set: ""},
            password_confirmation: {$set: ""},
          }),
          errors: errors,
        });
      }
      console.log(error);
    });
  }

  render() {
    return signupTemplate.call(this)
  }
}

const mapStateToProps = state => ({ 
  user: state.user
});

export default connect(mapStateToProps, {
  setCurrentUser,
})(Signup);