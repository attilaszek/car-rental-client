import React, {Component} from 'react'
import carTemplate from '../templates/Car.js'

import myAxios from '../MyAxios.js'

class Car extends Component {

  state = {
    car: null,
    viewport: {
      width: 300,
      height: 250,
      latitude: 20,
      longitude: 0,
      zoom: 0
    },
  }

  getCar = (id) => {
    const self = this
    myAxios.get('cars/show.json', {
      params: {
        id: id
      }
    }).then(function (response) {
      console.log(response)
      self.setState({
        car: response.data
      })
    })
  }

  onStarClick = (id, nextValue) => {
    const self = this
    myAxios.post('ratings.json', {
      car_id: id,
      rate: nextValue
    }).then(function (response) {
      console.log(response);
      self.getCar(id)
    })
  }

  componentWillMount() {
    this.getCar(this.props.match.params.id)
  }

  render() {
    if (!this.state.car) {
      return <h4>Loading...</h4>
    }
    return carTemplate.call(this)
  }
}

export default Car