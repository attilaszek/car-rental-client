import React, { Component } from 'react'
import reservationsSubTemplate from '../templates/ReservationsSub.js'

import myAxios from '../MyAxios.js'

class ReservationsSub extends Component {

  state = {
    reservations: []
  }

  loadReservations = () => {
    const self = this
    myAxios.get('reservations/index.json', {
      params: {
        user_id: this.props.user_id,
        car_id: this.props.car_id
      }
    })
    .then(function (response) {
      console.log(response)
      self.setState({
        reservations: response.data
      })
    })
  }

  componentWillMount() {
    this.loadReservations()
  }

  render() {
    return reservationsSubTemplate.call(this)
  }
}

export default ReservationsSub;