import React, { Component } from 'react';
import myAxios from '../MyAxios.js'
import signupTemplate from '../templates/Signup.js'

import { setCurrentUser } from '../actions/index.js'
import { connect } from 'react-redux';
import update from 'react-addons-update'
import { Redirect } from 'react-router-dom'

class Signup extends Component {
  state = {
    user: {
      id: null,
      first_name: "",
      last_name: "",
      email: "",
    },
    errors: {
      first_name: [],
      last_name: [],
      email: [],
      password: [],
      password_confirmation: []
    },
    redirect: false,
    new_user: false,
  }

  handleInputChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      user: update(this.state.user, {
        [name]: {$set: value}
      })
    });
  }

  renderRedirect = () => {
    if (this.state.redirect) {
      return <Redirect to='/users' />
    }
  }

  handleClick = () => {

    const self = this

    myAxios.put('users/update.json', this.state.user)
    .then(function (response) {
      var auth_token = response.data.token
      if (self.props.user) {
        self.setState({
          redirect: true
        })
      }
      else {
        self.login(auth_token)
      }
      console.log(response)
    })
    .catch(function (error) {
      if (error.response) {
        var errors = error.response.data
        console.log(errors)
        self.setState({
          user: update(self.state.user, {
            password: {$set: ""},
            password_confirmation: {$set: ""},
          }),
          errors: errors,
        });
      }
      console.log(error);
    });
  }

  getUser = (id) => {
    const self = this
    myAxios.get('users/show.json', {
      params: {
        id: id
      }
    }).then(function (response) {
      console.log(response)
      self.setState({
        user: update(self.state.user, {
          id: {$set: response.data.id},
          first_name: {$set: response.data.first_name},
          last_name: {$set: response.data.last_name},
          email: {$set: response.data.email},
        }),
      })
    })
  }

  componentWillMount() {
    this.getUser(this.props.match.params.id)
  }

  render() {
    return signupTemplate.call(this)
  }
}

const mapStateToProps = state => ({ 
  user: state.user
});

export default connect(mapStateToProps, {
  setCurrentUser,
})(Signup);