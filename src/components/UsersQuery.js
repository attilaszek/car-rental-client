import React, {Component} from 'react'
import usersQueryTemplate from '../templates/UsersQuery.js'

import myAxios from '../MyAxios.js'

class UsersQuery extends Component {

  state = {
    users: []
  }

  getUsersList = () => {
    const self = this
    myAxios.get('query/user_list.json')
    .then(function (response) {
      console.log(response)
      self.setState({
        users: response.data
      })
    })
  }

  componentWillMount() {
    this.getUsersList()
  }

  render() {
    return usersQueryTemplate.call(this)
  }
}

export default UsersQuery