import React, {Component} from 'react'
import newCarTemplate from '../templates/NewCar.js'

import myAxios from '../MyAxios.js'

import { connect } from 'react-redux';

import update from 'react-addons-update'

class NewCar extends Component {

  state = {
    colors: [],
    car: {
      name: "",
      color: "",
      weight: "",
      latitude: "",
      longitude: "",
    },
    image: null,
    errors: [],
    fireRedirect: false
  }

  getColors = () => {
    myAxios.get('cars/colors.json')
    .then(response => {
      console.log(response)
      this.setState({
        colors: Object.keys(response.data)
      })
    })
  }

  handleInputChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      car: update(this.state.car, {
        [name]: {$set: value}
      })

    })
  }

  handleSubmit = (event) => {

    let formData = new FormData()
    for (var key in this.state.car) {
      formData.append(key, this.state.car[key])
    }
    formData.append("image", this.state.image)

    const self = this
    myAxios.post('cars/new.json',
      formData,
      {
        headers: {
            'Content-Type': 'multipart/form-data'
        }
      }
    ).then(function (response) {
      console.log(response);
      self.setState({
        fireRedirect: true
      })
    }).catch(function (error) {
      if (error.response) {
        var errors = error.response.data
        console.log(errors)
        self.setState({
          errors: errors
        })
      }
    });
  }

  handleFileChange = (event) => {
    console.log(event.target.files[0])

    this.setState({
      image: event.target.files[0]
    })
  }

  componentWillMount() {
    this.getColors()
  }

  render() {
    return newCarTemplate.call(this)
  }
}

export default NewCar