import React, {Component} from 'react'
import carsQueryTemplate from '../templates/CarsQuery.js'

import myAxios from '../MyAxios.js'

class CarsQuery extends Component {

  state = {
    cars: [],
    users: [],
    user_id: ""
  }

  getCarsList = () => {
    const self = this
    myAxios.get('query/reserved_cars.json', {
      params: {
        user_id: this.state.user_id
      }
    })
    .then(function (response) {
      console.log(response)
      self.setState({
        cars: response.data
      })
    })
  }

  getUsersList = () => {
    const self = this
    myAxios.get('users/index.json')
    .then(function (response) {
      console.log(response)
      self.setState({
        users: response.data
      })
    })
  }

  handleInputChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    }, this.getCarsList)
  }

  componentWillMount() {
    this.getCarsList()
    this.getUsersList()
  }

  render() {
    return carsQueryTemplate.call(this)
  }
}

export default CarsQuery