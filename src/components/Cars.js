import React, {Component} from 'react'
import carsTemplate from '../templates/Cars.js'

import myAxios from '../MyAxios.js'

import { populateCarsList, updateCar, deleteCar } from '../actions/index.js'
import { connect } from 'react-redux';

class Cars extends Component {

  state = {
    colors: [],
    name: "",
    color: "",
    weight_min: 0,
    weight_max: 2000,
    start_date: "",
    end_date: "",
    // start_date: new Date().toJSON().slice(0,10),
    // end_date: new Date().toJSON().slice(0,10),
    viewport: {
      width: 840,
      height: 250,
      latitude: 20,
      longitude: 0,
      zoom: 0
    },
    start_date_error: "",
    end_date_error: "",
  }

  getCar = (id) => {
    const self = this
    myAxios.get('cars/show.json', {
      params: {
        id: id 
      }
    }).then(function (response) {
      console.log(response)
      self.props.updateCar(response.data)
    })
  }

  deleteCar = (id) => {
    const self = this
    myAxios.delete('cars.json', {
      params: {
        id: id
      }
    }).then(function (response) {
      console.log(response)
      self.props.deleteCar(response.data)
    })
  }

  reserveCar = (id) => {
    const self = this
    myAxios.post('reservations/new.json', {
      car_id: id,
      start_date: this.state.start_date,
      end_date: this.state.end_date
    }).then(function (response) {
      console.log(response)
      self.loadFilteredCars()
    })
    .catch(function (error) {
      if (error.response) {
        var errors = error.response.data
        self.setState({
          start_date_error: errors.start_date,
          end_date_error: errors.end_date
        })
      }
    })
  }

  getColors = () => {
    myAxios.get('cars/colors.json')
    .then(response => {
      console.log(response)
      this.setState({
        colors: Object.keys(response.data)
      })
    })
  }

  onStarClick = (id, nextValue) => {
    const self = this
    myAxios.post('ratings.json', {
      car_id: id,
      rate: nextValue
    }).then(function (response) {
      console.log(response);
      self.getCar(id)
    })
  }

  handleInputChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    }, this.loadFilteredCars);
  }

  loadFilteredCars = () => {
    const self = this
    myAxios.get('query/filter_cars.json', {
      params: {
        name: this.state.name,
        color: this.state.color ? this.state.color : null,
        weight_min: this.state.weight_min,
        weight_max: this.state.weight_max,
        start_date: this.state.start_date,
        end_date: this.state.end_date
      }
    })
    .then(response => {
      self.props.populateCarsList(response.data)
      self.setState({
        start_date_error: "",
        end_date_error: ""
      })
    })
    .catch(function (error) {
      if (error.response) {
        var errors = error.response.data
        console.log(errors)
      }
    })
  }

  componentWillMount() {
    this.loadFilteredCars()
    this.getColors()
  }

  render() {
    return carsTemplate.call(this)
  }
}

const mapStateToProps = state => ({ 
  cars: Array.from(state.cars),
  user: state.user
});

export default connect(mapStateToProps, {
  populateCarsList,
  updateCar,
  deleteCar
})(Cars);