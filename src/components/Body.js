import React, { Component } from 'react'
import '../styles/Body.css'

import Home from '../templates/Home.js'
import Signup from './Signup.js'
import Cars from './Cars.js'
import Car from './Car.js'
import NewCar from './NewCar.js'
import EditCar from './EditCar.js'
import Reservations from './Reservations.js'
import UsersQuery from './UsersQuery.js'
import CarsQuery from './CarsQuery.js'
import Users from './Users.js'
import EditUser from './EditUser.js'

import PrivateRoute from '../guards/PrivateRoute.js'
import AdminRoute from '../guards/AdminRoute.js'
import GuestRoute from '../guards/GuestRoute.js'

import { connect } from 'react-redux';
import { Switch, Route, withRouter } from 'react-router-dom'

import myAxios from '../MyAxios.js'
import { setCurrentUser } from '../actions/index.js'


class Body extends Component {
  getCurrentUser = () => {
    const self = this
    myAxios.get('users/get_current_user.json')
    .then(response => {
      console.log(response)
      self.props.setCurrentUser(response.data)
    })
  }
  
  componentWillMount(){
    if (sessionStorage.getItem('jwtToken')) {
      this.getCurrentUser();
    }
  }

  render() {
    if (sessionStorage.getItem('jwtToken') && !this.props.user) {
      return <p>Loading current user... Please wait</p>
    }
    return(
      <div className="my-container">
        <Switch>
          <Route exact path='/' component={Home}/>
          <GuestRoute exact path='/signup' component={Signup}/>

          <PrivateRoute exact path='/cars' component={Cars}/>
          <AdminRoute exact path='/cars/new' component={NewCar}/>
          <AdminRoute exact path='/cars/edit/:id' component={EditCar}/>
          <PrivateRoute exact path='/cars/:id' component={Car}/>
          <PrivateRoute exact path='/reservations' component={Reservations}/>
          <AdminRoute exact path='/query/users' component={UsersQuery}/>
          <AdminRoute exact path='/query/cars' component={CarsQuery}/>
          <AdminRoute exact path='/users' component={Users}/>
          <AdminRoute exact path='/users/new' component={Signup}/>
          <AdminRoute exact path='/users/edit/:id' component={EditUser}/>
        </Switch>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return { user: state.user };
};

export default withRouter(connect(mapStateToProps, {
  setCurrentUser
})(Body));