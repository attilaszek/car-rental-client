import React, {Component} from 'react'
import editCarTemplate from '../templates/EditCar.js'

import myAxios from '../MyAxios.js'

import update from 'react-addons-update'

class EditCar extends Component {

  state = {
    colors: [],
    car: null,
    image: null,
    errors: [],
    fireRedirect: false
  }

  getColors = () => {
    myAxios.get('cars/colors.json')
    .then(response => {
      console.log(response)
      this.setState({
        colors: Object.keys(response.data)
      })
    })
  }

  getCar = (id) => {
    const self = this
    myAxios.get('cars/show.json', {
      params: {
        id: id
      }
    }).then(function (response) {
      console.log(response)
      self.setState({
        car: response.data
      })
    })
  }

  handleInputChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      car: update(this.state.car, {
        [name]: {$set: value}
      })

    })
  }

  handleFileChange = (event) => {
    this.setState({
      image: event.target.files[0]
    })
  }

  handleSubmit = (event) => {
    const self = this

    let formData = new FormData()
    for (var key in this.state.car) {
      formData.append(key, this.state.car[key])
    }
    formData.delete("image")
    if (this.state.image) formData.append("image", this.state.image)

    myAxios.put('cars/edit.json',
      formData,
      {
        headers: {
            'Content-Type': 'multipart/form-data'
        }
      }
    ).then(function (response) {
      console.log(response);
      self.setState({
        fireRedirect: true
      })
    }).catch(function (error) {
      if (error.response) {
        var errors = error.response.data
        console.log(errors)
        self.setState({
          errors: errors
        })
      }
    });
  }

  componentWillMount() {
    this.getColors()
    this.getCar(this.props.match.params.id)
  }

  render() {
    if (!this.state.car) {
      return <h4>Loading...</h4>
    }
    return editCarTemplate.call(this)
  }
}

export default EditCar