import * as types from '../constants/ActionTypes'

export const setCurrentUser = (user) => ({
  type: types.SET_CURRENT_USER,
  payload: user
})

export const populateCarsList = (cars) => ({
  type: types.POPULATE_CARS_LIST,
  payload: cars
})

export const updateCar = (car) => ({
  type: types.UPDATE_CAR,
  payload: car
})

export const deleteCar = (car) => ({
  type: types.DELETE_CAR,
  payload: car
})