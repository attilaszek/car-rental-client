import * as types from '../constants/ActionTypes'

const cars = (state = {}, action) => {
  switch (action.type) {
    case types.POPULATE_CARS_LIST:
      return action.payload
    case types.UPDATE_CAR:
      var new_state = state.slice()
      var index = state.findIndex(car => car.id == action.payload.id)
      new_state[index] = action.payload
      return new_state
    case types.DELETE_CAR:
      var new_state = state.slice()
      var index = state.findIndex(car => car.id == action.payload.id)
      new_state.splice(index, 1)
      return new_state
    default:
      return state
  }
}

export default cars