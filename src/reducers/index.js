import { combineReducers } from "redux"
import user from "./user"
// import users from "./users"
import cars from "./cars"

const car_rental = combineReducers({
  user: user,
  // users: users
  cars: cars
});

export default car_rental