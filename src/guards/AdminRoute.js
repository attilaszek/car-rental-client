import React from 'react'
import { Route, Redirect, withRouter } from 'react-router-dom'

import { connect } from 'react-redux'

const AdminRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}

    render={props =>
      sessionStorage.getItem('jwtToken') && rest.user.admin ? (
        <Component {...props} />
      ) : (
        <Redirect
          to={{
            pathname: "/signup",
            state: { from: props.location }
          }}
        />
      )
    }
  />
);

const mapStateToProps = state => {
  return { user: state.user };
};

export default withRouter(connect(mapStateToProps)(AdminRoute));