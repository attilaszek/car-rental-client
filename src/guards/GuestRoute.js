import React from 'react'
import { Route, Redirect } from 'react-router-dom'

const GuestRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      !sessionStorage.getItem('jwtToken') ? (
        <Component {...props} />
      ) : (
        <Redirect
          to={{
            pathname: "/",
            state: { from: props.location }
          }}
        />
      )
    }
  />
);

export default GuestRoute
