import React from 'react'
import { Route, Redirect } from 'react-router-dom'

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}

    render={props =>
      sessionStorage.getItem('jwtToken') ? (
        <Component {...props} />
      ) : (
        <Redirect
          to={{
            pathname: "/signup",
            state: { from: props.location }
          }}
        />
      )
    }
  />
);

export default PrivateRoute
